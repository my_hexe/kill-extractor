# Kill Extractor

## Installation

1. Install Python Packages from `requirements.txt`
    ```bash
    pip3 install -r requirements.txt
    ```

2. Download git lfs files
    ```bash
    git lfs pull
    ```

3. Setup environment

    1. Copy `.env-example` to `.env`
        ```bash
        cp .env-example .env
        ```
    2. Set your API keys in `.env`

## Usage

**Get the help message**
```bash
python3 -m kill_extractor --help
```
