FROM python:3.8-slim-buster

# Install FFMPEG
RUN apt-get update && \
    apt-get install --no-install-recommends -y ffmpeg && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# Install Python Dependencies
COPY requirements.txt /tmp/requirements.txt
RUN pip install --no-cache-dir -r /tmp/requirements.txt

# Copy the source code
WORKDIR /app
COPY . /app

ENTRYPOINT ["python", "-m", "kill_extractor"]
