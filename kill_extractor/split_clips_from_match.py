import os
from typing import Iterable, Optional, Set

import cv2
import numpy as np
import skimage.metrics
from tqdm import tqdm
from valo_api_official.responses.match_details import MatchDetailsV1

from kill_extractor.clip_cutters.base_clip_cutter import BaseClipCutter
from kill_extractor.config import Config
from kill_extractor.utils.image import cut_image, filter_red
from kill_extractor.utils.video import split_video


def has_spike(image: np.ndarray) -> bool:
    image = image.copy()
    image = filter_red(image, Config.BLUE_THR, Config.GREEN_THR, Config.RED_THR)
    error = skimage.metrics.mean_squared_error(image, Config.SPIKE_TEMPLATE)
    return error < Config.MSE_ERROR_THRESHOLD


def get_first_plant_time_from_video(video_path: str, skip_first_millis: int = 0) -> int:
    video = cv2.VideoCapture(video_path)

    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    length = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    fps = video.get(cv2.CAP_PROP_FPS)

    skip_frames = skip_first_millis // 1000 * fps

    x_start, x_end = int((width - Config.SPIKE_WIDTH) // 2), int(
        (width + Config.SPIKE_WIDTH) // 2
    )
    y_start, y_end = Config.SPIKE_Y, Config.SPIKE_Y + Config.SPIKE_HEIGHT

    frames_with_spike, wait_for_toggle = 0, False
    for frame in tqdm(range(length), desc="Searching first plant", leave=False):
        success, image = video.read()
        if not success:
            break
        if frame < skip_frames:
            continue

        spike_region = cut_image(image, (x_start, x_end), (y_start, y_end))
        if not has_spike(spike_region):
            frames_with_spike, wait_for_toggle = 0, False
            continue

        if frame < 2 * fps:
            wait_for_toggle = True

        if wait_for_toggle:
            continue

        frames_with_spike += 1
        if frames_with_spike > 2 * fps:
            video.release()
            return int(1000 * (frame - frames_with_spike) // fps)


def get_first_plant_time_from_match(match: MatchDetailsV1) -> int:
    first_plant_round = [r for r in match.roundResults if r.plantRoundTime > 0][0]
    kill = next(k for player in first_plant_round.playerStats for k in player.kills)
    round_since_game_starts = kill.timeSinceGameStartMillis - kill.timeSinceRoundStartMillis
    return round_since_game_starts + first_plant_round.plantRoundTime


def calculate_video_offset(video_path: str, match: MatchDetailsV1) -> Optional[int]:
    first_plant_in_match = get_first_plant_time_from_match(match)
    tqdm.write(f"Plant time in match was: {first_plant_in_match}ms")

    first_plant_in_video = get_first_plant_time_from_video(
        video_path, skip_first_millis=first_plant_in_match
    )
    tqdm.write(f"Plant time in video was: {first_plant_in_video}ms")

    if first_plant_in_video is None or first_plant_in_match is None:
        tqdm.write(f"Could not find first plant for {match.matchInfo.matchId}")
        return

    return first_plant_in_video - first_plant_in_match


def split_clips_from_match(
    clip_cutters: Iterable[BaseClipCutter],
    video_path: str,
    match: MatchDetailsV1,
    puuids: Iterable[str],
    output_folder: str,
    clip_offset: int = Config.DEFAULT_CLIP_OFFSET,
) -> Set[str]:
    offset = calculate_video_offset(video_path, match)
    if offset is None:
        return set()

    clips = set()
    for clip_cutter in clip_cutters:
        clip_slots = clip_cutter(match, puuids)
        for i, clip_slot in enumerate(clip_slots):
            os.makedirs(output_folder, exist_ok=True)
            clip = os.path.join(output_folder, f"{clip_cutter.FILE_PREFIX}_{i}.mp4")
            start = (clip_slot[0] + offset) // 1000 - clip_offset
            end = (clip_slot[1] + offset) // 1000 + clip_offset
            split_video(video_path, start, end, clip)
            clips.add(clip)
    return clips
