import os

import cv2
import numpy as np
import optuna
import skimage
from optuna import Trial

from kill_extractor.utils.image import cut_image, filter_red

SPIKE_TEMPLATE = filter_red(cv2.imread("res/spike.png"))
SPIKE_Y = 14
SPIKE_WIDTH, SPIKE_HEIGHT = 70, 70

with_spike = [
    (os.path.join("dataset/with_spike", s), True) for s in os.listdir("dataset/with_spike")
]
without_spike = [
    (os.path.join("dataset/without_spike", s), False)
    for s in os.listdir("dataset/without_spike")
]

np.random.shuffle(with_spike)
np.random.shuffle(without_spike)
datapoints = min(len(with_spike), len(without_spike))
mixed = with_spike[:datapoints] + without_spike[:datapoints]
np.random.shuffle(mixed)


def has_spike(
    image: np.ndarray, blue_thr: float = 100, green_thr: float = 100, red_thr: float = 180
) -> float:
    image = image.copy()
    image = filter_red(image, blue_thr, green_thr, red_thr)
    error = skimage.metrics.mean_squared_error(image, SPIKE_TEMPLATE)
    return error


def calculate_score(blue_thr, green_thr, red_thr, error_threshold) -> float:
    correct = 0
    for path, spike in mixed:
        image = cv2.imread(path)
        height, width, _ = image.shape
        x_start, x_end = int((width - SPIKE_WIDTH) // 2), int((width + SPIKE_WIDTH) // 2)
        y_start, y_end = SPIKE_Y, SPIKE_Y + SPIKE_HEIGHT
        spike_region = cut_image(image, (x_start, x_end), (y_start, y_end))
        error = has_spike(spike_region, blue_thr, green_thr, red_thr)
        if spike and error < error_threshold:
            correct += 1
        elif not spike and error > error_threshold:
            correct += 1
    return correct / len(mixed)


def calculate_error(trial: Trial) -> float:
    blue_thr, green_thr, red_thr = (
        trial.suggest_int("blue_thr", 0, 255),
        trial.suggest_int("green_thr", 0, 255),
        trial.suggest_int("red_thr", 0, 255),
    )
    error_threshold = trial.suggest_int("error_threshold", 1000, 5000, 10)
    return calculate_score(blue_thr, green_thr, red_thr, error_threshold)


if __name__ == "__main__":
    study = optuna.create_study(direction="maximize")
    study.optimize(
        calculate_error,
        n_trials=100,
        n_jobs=os.cpu_count(),
        show_progress_bar=True,
    )
    print(study.best_value)
    print(study.best_params)
