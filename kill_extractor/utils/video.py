import cv2
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip

from kill_extractor.utils.hide_prints import HidePrints


def split_video(video_path: str, start: int, end: int, output_path: str):
    with HidePrints():
        ffmpeg_extract_subclip(
            video_path,
            start,
            end,
            targetname=output_path,
        )


def get_video_duration_millis(video_path: str) -> int:
    video = cv2.VideoCapture(video_path)
    duration = int(video.get(cv2.CAP_PROP_FRAME_COUNT)) / video.get(cv2.CAP_PROP_FPS)
    video.release()
    return 1000 * duration
