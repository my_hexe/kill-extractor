import os
from datetime import datetime

import requests
import tzlocal
from dotenv import load_dotenv

load_dotenv()


def get_twitch_video_start_millis(video_id: int) -> int:
    api_url = f"https://api.twitch.tv/helix/videos?id={video_id}"
    headers = {
        "Client-ID": os.environ["TWITCH_CLIENT_ID"],
        "Authorization": f"Bearer {os.environ['TWITCH_ACCESS_TOKEN']}",
    }
    video_data = requests.get(api_url, headers=headers).json()["data"]
    date = datetime.strptime(video_data[0]["created_at"], "%Y-%m-%dT%H:%M:%SZ")
    tz_offset = tzlocal.get_localzone().utcoffset(datetime.now())
    return int(date.timestamp() + tz_offset.seconds) * 1000


def download_twitch_video(video_id: int, output_folder: str) -> str:
    output = os.path.join(output_folder, f"{video_id}.mp4")
    if os.path.exists(output):
        return output

    os.makedirs(output_folder, exist_ok=True)
    os.system(f"twitch-dl download -k -q source -f mp4 -o {output} {video_id}")
    return output
