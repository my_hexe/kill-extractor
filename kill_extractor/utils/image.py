from typing import Tuple

import numpy as np


def filter_red(
    img: np.ndarray, blue_thr: float = 100, green_thr: float = 100, red_thr: float = 180
) -> np.ndarray:
    img = img.copy()
    idx = (img[:, :, 0] > blue_thr) | (img[:, :, 1] > green_thr) | (img[:, :, 2] < red_thr)
    img[idx] = (0, 0, 0)
    return img


def cut_image(
    image: np.ndarray, x_range: Tuple[int, int], y_range: Tuple[int, int]
) -> np.ndarray:
    return image[y_range[0] : y_range[1], x_range[0] : x_range[1]]
