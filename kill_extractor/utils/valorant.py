from typing import Iterable, List, Set

import valo_api
import valo_api_official
from cache_to_disk import cache_to_disk
from dotenv import load_dotenv
from tqdm import tqdm
from valo_api.endpoints.raw import EndpointType
from valo_api_official.responses.match_details import MatchDetailsV1

load_dotenv()


@cache_to_disk()
def get_match_data(region: str, match_id: str) -> MatchDetailsV1:
    return valo_api_official.get_match_details_v1(region, match_id)


@cache_to_disk()
def official_to_unofficial(puuid: str) -> str:
    account = valo_api_official.get_account_by_puuid_v1(puuid)
    name, tag = account.gameName, account.tagLine
    return valo_api.get_account_details_by_name_v1(name, tag).puuid


def get_match_ids_in_time(puuid: str, start_millis: int, end_millis: int) -> Set[str]:
    match_ids = set()
    puuid = official_to_unofficial(puuid)
    query_args = {"startIndex": 0, "endIndex": 20}
    while True:
        match_history = valo_api.get_raw_data_v1(
            EndpointType.MATCH_HISTORY, region="eu", value=puuid, queries=query_args
        )
        too_early = False
        for match in match_history.History:
            if match.QueueID != "competitive":
                continue
            if end_millis < match.GameStartTime:
                continue
            if match.GameStartTime < start_millis:
                too_early = True
                continue
            match_ids.add(match.MatchID)
        if too_early or query_args["endIndex"] >= match_history.Total:
            break
        query_args = {
            "startIndex": min(query_args["endIndex"] + 1, match_history.Total - 1),
            "endIndex": min(query_args["endIndex"] + 20, match_history.Total),
        }
    return match_ids


def get_puuid(name: str, tag: str) -> str:
    return valo_api_official.get_account_by_name_v1(name, tag).puuid


def get_matches(
    puuids: Iterable[str], start_millis: int, end_millis: int
) -> List[MatchDetailsV1]:
    match_ids = {
        m
        for puuid in puuids
        for m in get_match_ids_in_time(puuid, start_millis, end_millis)
    }
    return sorted(
        (get_match_data("eu", m) for m in tqdm(match_ids, desc="Getting match data")),
        key=lambda m: m.matchInfo.gameStartMillis,
    )
