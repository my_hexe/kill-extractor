from valo_api_official.responses.match_details import MatchDetailsV1

from kill_extractor.config import Config
from kill_extractor.utils.video import split_video


def split_match(
    match: MatchDetailsV1,
    output: str,
    video_path: str,
    video_start_millis: int,
    match_end_time_threshold: int = Config.DEFAULT_MATCH_END_TIME_THRESHOLD,
) -> str:
    start = match.matchInfo.gameStartMillis - video_start_millis
    end = start + match.matchInfo.gameLengthMillis + match_end_time_threshold
    split_video(video_path, round(start / 1000), round(end / 1000), output)
    return output
