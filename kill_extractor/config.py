import cv2

from kill_extractor.utils.image import filter_red


class Config:
    DEFAULT_CLIP_OFFSET = 5
    """The default clip offset for all kill clips in seconds.

    The clip offset will be cut before the kill and after the kill.
    """
    DEFAULT_MATCH_END_TIME_THRESHOLD = 60 * 3000
    """The default threshold for the end of a match in milliseconds
    """

    SPIKE_Y = 14
    """The y coordinate of the spike icon from the top"""
    SPIKE_WIDTH = 70
    """The width of the spike icon"""
    SPIKE_HEIGHT = 70
    """The height of the spike icon"""
    SPIKE_TEMPLATE = filter_red(cv2.imread("res/spike.png"))
    """The spike icon template"""

    BLUE_THR = 83
    """The blue threshold for the spike detection.

    This value can be optimized with kill_extractor/utils/spike_detector.py
    """
    GREEN_THR = 3
    """The green threshold for the spike detection.

    This value can be optimized with kill_extractor/utils/spike_detector.py
    """
    RED_THR = 144
    """The red threshold for the spike detection.

    This value can be optimized with kill_extractor/utils/spike_detector.py
    """
    MSE_ERROR_THRESHOLD = 3090
    """The mean squared error threshold for the spike detection.

    This value can be optimized with kill_extractor/utils/spike_detector.py
    """
