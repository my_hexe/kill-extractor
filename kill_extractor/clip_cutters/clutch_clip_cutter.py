from typing import Iterable, Tuple, Union

from valo_api_official.responses.match_details import MatchDetailsV1

from kill_extractor.clip_cutters.base_clip_cutter import BaseClipCutter


class ClutchClipCutter(BaseClipCutter):
    FILE_PREFIX = "clutch"

    def get_clip_slots(
        self, match: MatchDetailsV1, puuids: Iterable[str]
    ) -> Iterable[Union[int, Tuple[int, int]]]:
        clutch_rounds = (
            r for r in match.roundResults if r.roundCeremony == "CeremonyClutch"
        )
        for round in clutch_rounds:
            all_victims = {k.victim for p in round.playerStats for k in p.kills}
            if any(puuid in all_victims for puuid in puuids):
                continue

            player = next(player for player in round.playerStats if player.puuid in puuids)
            if len(player.kills) == 0:
                continue

            first_kill, last_kill = player.kills[0], player.kills[-1]
            yield first_kill.timeSinceGameStartMillis, last_kill.timeSinceGameStartMillis
