from abc import ABC, abstractmethod
from argparse import ArgumentParser
from typing import Iterable, Tuple, Union

from valo_api_official.responses.match_details import MatchDetailsV1


class BaseClipCutter(ABC):
    FILE_PREFIX = "clip"

    def __init__(self, **kwargs):
        super().__init__()

    @classmethod
    def add_arguments(cls, parser: ArgumentParser):
        pass

    @abstractmethod
    def get_clip_slots(
        self, match: MatchDetailsV1, puuids: Iterable[str]
    ) -> Iterable[Union[int, Tuple[int, int]]]:
        ...

    def __call__(
        self, match: MatchDetailsV1, puuids: Iterable[str]
    ) -> Iterable[Tuple[int, int]]:
        for slot in self.get_clip_slots(match, puuids):
            if isinstance(slot, int):
                yield slot, slot
            else:
                yield slot
