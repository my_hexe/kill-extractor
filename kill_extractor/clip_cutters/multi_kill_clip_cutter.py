from argparse import ArgumentParser
from typing import Iterable, Tuple, Union

from valo_api_official.responses.match_details import MatchDetailsV1

from kill_extractor.clip_cutters.base_clip_cutter import BaseClipCutter


class MultiKillClipCutter(BaseClipCutter):
    FILE_PREFIX = "multi_kill"
    max_time_between_kills: int = 5000
    """Maximum time between two kills in milliseconds.

    If larger than that, the clip will be split into two clips.
    """
    min_amount_kills: int = 2
    """Minimum amount of kills in a multi kill clip."""

    def __init__(self, **kwargs):
        self.max_time_between_kills = kwargs["max_time_between_kills"]
        self.min_amount_kills = kwargs["min_amount_kills"]
        super().__init__(**kwargs)

    @classmethod
    def add_arguments(cls, parser: ArgumentParser):
        parser.add_argument(
            "--max-time-between-kills",
            type=int,
            default=cls.max_time_between_kills,
            help=(
                "Maximum time between two kills in milliseconds. "
                "If larger than that, the clip will be split into two clips."
            ),
        )
        parser.add_argument(
            "--min-amount-kills",
            type=int,
            default=cls.min_amount_kills,
            help="Minimum amount of kills in a multi kill clip.",
        )

    def get_clip_slots(
        self, match: MatchDetailsV1, puuids: Iterable[str]
    ) -> Iterable[Union[int, Tuple[int, int]]]:
        for round in match.roundResults:
            for player in round.playerStats:
                if player.puuid not in puuids:
                    continue
                kills = sorted(player.kills, key=lambda k: k.timeSinceGameStartMillis)
                if len(kills) == 0:
                    continue

                multi_kill = [kills[0]]
                for kill in kills[1:]:
                    last_kill = multi_kill[-1]
                    time_0 = last_kill.timeSinceGameStartMillis
                    time_1 = kill.timeSinceGameStartMillis

                    if abs(time_0 - time_1) <= self.max_time_between_kills:
                        multi_kill.append(kill)
                        continue

                    if len(multi_kill) >= self.min_amount_kills:
                        first, *_, last = multi_kill
                        yield first.timeSinceGameStartMillis, last.timeSinceGameStartMillis
                        continue

                    multi_kill = [kill]

                if len(multi_kill) >= self.min_amount_kills:
                    first, *_, last = multi_kill
                    yield first.timeSinceGameStartMillis, last.timeSinceGameStartMillis
