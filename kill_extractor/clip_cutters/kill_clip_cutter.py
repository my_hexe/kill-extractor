from typing import Iterable, Tuple, Union

from valo_api_official.responses.match_details import MatchDetailsV1

from kill_extractor.clip_cutters.base_clip_cutter import BaseClipCutter


class KillClipCutter(BaseClipCutter):
    FILE_PREFIX = "kill"

    def get_clip_slots(
        self, match: MatchDetailsV1, puuids: Iterable[str]
    ) -> Iterable[Union[int, Tuple[int, int]]]:
        all_player_stats = (p for r in match.roundResults for p in r.playerStats)
        player_stats = filter(lambda p: p.puuid in puuids, all_player_stats)
        all_kills = (k for p in player_stats for k in p.kills)
        return (k.timeSinceGameStartMillis for k in all_kills)
