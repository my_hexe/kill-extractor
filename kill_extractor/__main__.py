import os
from argparse import ArgumentParser
from tempfile import NamedTemporaryFile

import numpy as np
from moviepy.video.compositing.concatenate import concatenate_videoclips
from moviepy.video.io.VideoFileClip import VideoFileClip
from tqdm import tqdm

from kill_extractor import clip_cutters
from kill_extractor.config import Config
from kill_extractor.split_clips_from_match import split_clips_from_match
from kill_extractor.split_video_by_match import split_match
from kill_extractor.utils.hide_prints import HidePrints
from kill_extractor.utils.twitch import download_twitch_video, get_twitch_video_start_millis
from kill_extractor.utils.valorant import get_matches, get_puuid
from kill_extractor.utils.video import get_video_duration_millis

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-i", "--video-id", type=str, help="Twitch Video ID (See URL)")
    parser.add_argument("-v", "--video-path", type=str, help="Path to the input video file")
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        default="results",
        help="Where to save the clips",
    )
    parser.add_argument(
        "-s",
        "--video-start-millis",
        type=int,
        help="Start time of the video in milliseconds",
    )
    parser.add_argument(
        "-p",
        "--player",
        type=str,
        nargs="+",
        action="append",
        required=True,
        help="Riot-ID of the player(s), like this: 'ManuelHexe#5777'",
    )
    parser.add_argument(
        "-l",
        "--clip-length",
        type=int,
        default=Config.DEFAULT_CLIP_OFFSET,
        help="The clip length for all clips",
    )
    parser.add_argument(
        "-c",
        "--concatenate",
        action="store_true",
        help="Concatenate all clips into one video",
    )
    parser.add_argument(
        "--cutters",
        nargs="+",
        choices=[c.__name__ for c in clip_cutters.__all__],
        default=[c.__name__ for c in clip_cutters.__all__],
        help="Clip Cutters to use",
    )
    for c in clip_cutters.__all__:
        group_parser = parser.add_argument_group(c.__name__)
        c.add_arguments(group_parser)
    args = parser.parse_args()

    video_id, video_path, video_start_millis = (
        args.video_id,
        args.video_path,
        args.video_start_millis,
    )
    players, output, clip_length = args.player, args.output, args.clip_length
    clip_cutters = [getattr(clip_cutters, cutter)(**vars(args)) for cutter in args.cutters]

    if video_id is not None:
        video_path = download_twitch_video(video_id, "videos")
        video_start_millis = get_twitch_video_start_millis(video_id)
    elif video_path is None or video_start_millis is None:
        raise ValueError("Either video-id or video-path and video-start-millis must be set")

    video_end_millis = video_start_millis + get_video_duration_millis(video_path)

    puuids = {
        get_puuid(*p.split("#"))
        for ps in tqdm(players, desc="Getting player ids")
        for p in ps
    }
    matches = get_matches(puuids, video_start_millis, video_end_millis)
    print(f"Found {len(matches)} matches for the given players")

    all_clips = set()
    for match in tqdm(matches, desc="Processing Matches", position=1):
        tqdm.write("-" * 80)
        tqdm.write(f"Processing match {match.matchInfo.matchId}")

        output_match = os.path.join(output, match.matchInfo.matchId)
        os.makedirs(output_match, exist_ok=True)

        with NamedTemporaryFile(suffix=".mp4") as tf:
            tqdm.write("Splitting match from video")
            match_video = split_match(
                match,
                tf.name,
                video_path,
                video_start_millis,
            )
            tqdm.write("Splitting clips from match")
            match_clips = split_clips_from_match(
                clip_cutters,
                match_video,
                match,
                puuids,
                output_match,
            )
            tqdm.write(f"Extracted {len(match_clips)} clips")
            all_clips.update(match_clips)
        tqdm.write("-" * 80)

    if args.concatenate:
        output_all_clips = os.path.join(output, "all.mp4")
        tqdm.write(f"Concatenating all clips to: {output_all_clips}")
        with HidePrints():
            clips = [VideoFileClip(clip).set_duration(clip_length) for clip in all_clips]
            fps = round(np.array([clip.fps for clip in clips]).mean())
            clips = [clip.set_fps(fps) for clip in clips]
            all_clips = concatenate_videoclips(clips, method="compose")
            all_clips.write_videofile(output_all_clips, fps=fps, threads=os.cpu_count())
